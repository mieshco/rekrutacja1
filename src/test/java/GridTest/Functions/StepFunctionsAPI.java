package GridTest.Functions;

import GridTest.TestObjects.PackageDetails.CustomAttributes;
import GridTest.TestObjects.PackageDetails.TargetMachineDetail;
import GridTest.TestObjects.PackageDetails.TrackingDetails;
import GridTest.TestObjects.Package_;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StepFunctionsAPI {

    public Response sendRequestToAPI(String url) {
        return RestAssured.get(url);
    }

    public Package_ convertJsonToJavaObject(Response res) {
        String rawJson = res.getBody().asString();
        JSONObject root = new JSONObject(rawJson);
        JSONObject customAttributes = root.getJSONObject("custom_attributes");
        JSONObject targetMachineDetail = customAttributes.getJSONObject("target_machine_detail");
        JSONObject targetMachineDetail_location = targetMachineDetail.getJSONObject("location");
        JSONObject targetMachineDetail_address = targetMachineDetail.getJSONObject("address");
        JSONArray trackingDetails = root.getJSONArray("tracking_details");

        List<TrackingDetails> listOfTrackingDetails = new ArrayList<TrackingDetails>();
        for (int i = 0; i < trackingDetails.length(); i++) {
            JSONObject o = trackingDetails.getJSONObject(i);
            listOfTrackingDetails.add(new TrackingDetails(
                    o.getString("status"),
                    o.getString("origin_status"),
                    //   String.valueOf(o.getInt("agency")),
                    o.getString("datetime")));
        }

        return new Package_(
                root.getString("tracking_number"),
                root.getString("service"),
                root.getString("type"),
                root.getString("status"),
                new CustomAttributes(
                        customAttributes.getString("size"),
                        customAttributes.getString("target_machine_id"),
                        customAttributes.getBoolean("end_of_week_collection"),
                        new TargetMachineDetail(
                                targetMachineDetail.getString("name"),
                                targetMachineDetail.getString("opening_hours"),
                                targetMachineDetail.getString("location_description"),
                                targetMachineDetail_location.getDouble("latitude"),
                                targetMachineDetail_location.getDouble("longitude"),
                                targetMachineDetail_address.getString("line1"),
                                targetMachineDetail_address.getString("line2"),
                                // targetMachineDetail.getString("type"),
                                targetMachineDetail.getBoolean("location247")
                        )

                ),
                listOfTrackingDetails,
                //   root.getString("expected_flow"),
                root.getString("created_at"),
                root.getString("updated_at")
        );
    }
}
