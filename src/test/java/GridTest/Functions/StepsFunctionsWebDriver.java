package GridTest.Functions;

import GridTest.WebObjects.Element_;
import GridTest.WebObjects.StatusPage;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class StepsFunctionsWebDriver {

    public void openPage(WebDriver driver, String url) {
        driver.get(url);
    }

    public void CLICK(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            driver.findElement(By.xpath(element.getXpath())).click();
        } else {
            System.out.println("ELEMENT " + element.getName() + "NOT FOUND!");
        }
    }

    public boolean EXIST(WebDriver driver, Element_ element) {
        return driver.findElements(By.xpath(element.getXpath())).size() > 0;
    }

    public boolean EXIST_AFTER_TIMEOUT(WebDriver driver, Element_ element, Integer seconds) {
        WAIT(seconds);
        return driver.findElements(By.xpath(element.getXpath())).size() > 0;
    }

    public boolean EXIST_QUICK(WebDriver driver, Element_ element) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        return driver.findElements(By.xpath(element.getXpath())).size() != 0;
    }

    public int GET_SIZE_OF_LIST_OF_WEB_ELEMENTS(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            return driver.findElements(By.xpath(element.getXpath())).size();
        } else {
            return 0;
        }
    }

    public void INPUT(WebDriver driver, Element_ element, String value) {
        if (EXIST(driver, element)) {
            driver.findElement(By.xpath(element.getXpath())).sendKeys(value);
        }
    }

    public void CHECK(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            if (!driver.findElement(By.xpath(element.getXpath())).isSelected()) {
                driver.findElement(By.xpath(element.getXpath())).click();
            }
        }
    }

    public void UNCHECK(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            if (driver.findElement(By.xpath(element.getXpath())).isSelected()) {
                driver.findElement(By.xpath(element.getXpath())).click();
            }
        }
    }

    public String GET_TEXT(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            return driver.findElement(By.xpath(element.getXpath())).getText();
        }
        System.out.println("ELEMENT " + element.getName() + "NOT EXIST!");
        return "";
    }

    public void CLEAR_INPUT(WebDriver driver, Element_ element) {
        if (EXIST(driver, element)) {
            driver.findElement(By.xpath(element.getXpath())).clear();
        }
    }

    private void WAIT(int time) {
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Element_ getElementByName(WebDriver driver, String name) {
        for (Element_ e : StatusPage.values()) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        System.out.println("ELEMENT NAMED " + name + "NOT FOUND");
        return null;

    }

    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    public void takeScreenshoot(WebDriver driver) {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            File file = new File("C:/Users/" + System.getProperty("user.name") + "/recTaskOut");
            if (file.mkdir()) {
                System.out.println("New directory created: " + file.getPath());
            } else {
                System.out.println("File already exists or there is a problem");
            }
            File out = new File(file.getPath() + "/pic" + sdf.format(new Date()) + ".png");
            if (out.createNewFile()) {
                System.out.println("File created: " + out.getName());
            } else {
                System.out.println("File already exists.");
            }
            copyFileUsingStream(scrFile, out);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
