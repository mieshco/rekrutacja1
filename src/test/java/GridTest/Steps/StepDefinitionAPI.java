package GridTest.Steps;

import GridTest.Functions.StepFunctionsAPI;
import GridTest.TestObjects.Package_;
import cucumber.api.java.pl.Kiedy;
import cucumber.api.java.pl.Mając;
import cucumber.api.java.pl.Wtedy;
import io.restassured.response.Response;
import org.junit.Assert;

public class StepDefinitionAPI extends StepDefinition {
    private StepFunctionsAPI sfa;
    private Response res;
    private Package_ to;

    @Mając("^wszystkie dane potrzebne do wysłania zapytania poprzez API do serwisu shipx-proxy$")
    public void checkApiData() {
        sfa = new StepFunctionsAPI();

    }

    @Kiedy("^wysyłam zapytanie GET z numerem przesyłki (.*)$")
    public void sentGet(String packageNo) {
        res = sfa.sendRequestToAPI(Variables_.API_GET_URL + packageNo);
    }

    @Wtedy("^dostaję odpowiedź z serwisu ze statusem (\\d+)$")
    public void checkCode(int code) {
        Assert.assertEquals(code, res.getStatusCode());
    }

    @Wtedy("^odpowiedź jest w formie oczekiwanej$")
    public void responseIsInExpectedForm() {
        to = sfa.convertJsonToJavaObject(res);
    }

    @Wtedy("^przesyłka jest w statusie (.*)$")
    public void checkPackageStatus(String status) {
        Assert.assertEquals(status, to.getStatus());
    }
}
