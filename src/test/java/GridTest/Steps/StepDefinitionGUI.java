package GridTest.Steps;

import GridTest.Functions.StepsFunctionsWebDriver;
import GridTest.WebObjects.StartPage;
import GridTest.WebObjects.StatusPage;
import cucumber.api.java.pl.Wtedy;
import org.junit.Assert;

public class StepDefinitionGUI extends StepDefinition {
    private StepsFunctionsWebDriver sfwd;

    @cucumber.api.java.pl.Mając("^Otwartą stronę inpostu$")
    public void openInpostWebside() {
        sfwd = new StepsFunctionsWebDriver();
        DRIVER.get(Variables_.INPOST_URL);
        if (sfwd.EXIST_AFTER_TIMEOUT(DRIVER, StartPage.ONETRUST_POLICY_ACCEPT_BUTTON, 2)) {
            sfwd.CLICK(DRIVER, StartPage.ONETRUST_POLICY_ACCEPT_BUTTON);
        }
    }

    @cucumber.api.java.pl.Kiedy("^wyszukuję przesyłkę (.*)$")
    public void searchForPackageInfo(String packageNo) {
        sfwd.INPUT(DRIVER, StartPage.CONTAINER_SEARCH_INPUT, packageNo);
        sfwd.CLICK(DRIVER, StartPage.CONTAINER_SEARCH_BUTTON);
    }

    @Wtedy("^sprawdzam czy faktyczny status jest równy (.*)$")
    public void checkPackageStatus(String status) {
        sfwd.takeScreenshoot(DRIVER);
        Assert.assertEquals(status, sfwd.GET_TEXT(DRIVER, StatusPage.CURRENT_STATE));
    }

    @Wtedy("sprawdzam czy adres jest zgodny z oczekiwanym (.*)")
    public void checkURL(String url) {
        Assert.assertEquals(url, DRIVER.getCurrentUrl());
    }

    @Wtedy("^sprawdzam czy tekst elementu (.*) jest równy (.*)$")
    public void checkElementText(String name, String expectedValue) {
        Assert.assertEquals(sfwd.GET_TEXT(DRIVER, sfwd.getElementByName(DRIVER, name)), expectedValue);
    }

    @Wtedy("^sprawdzam obecnosc elementu (.*)$")
    public void checkElementExist(String name) {
        Assert.assertTrue(sfwd.EXIST(DRIVER, sfwd.getElementByName(DRIVER, name)));
    }
}
