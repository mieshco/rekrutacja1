package GridTest.Steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class HooksDefinition extends StepDefinition {


    @Before
    public void before() throws MalformedURLException {
        if (!System.getenv("BROWSER").equals("NONE")) {
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setPlatform(Platform.WINDOWS);
            MutableCapabilities options;
            if (System.getenv("BROWSER").equals("FIREFOX")) {
                cap.setBrowserName("firefox");
                options = new FirefoxOptions();
            } else {
                cap.setBrowserName("chrome");
                options = new ChromeOptions();
            }
            options.merge(cap);
            String hubUrl = "http://192.168.1.10:4444/wd/hub";
            DRIVER = new RemoteWebDriver(new URL(hubUrl), options);
        }
    }

    @After
    public void after() {
        if (!System.getenv("BROWSER").equals("NONE")) {
            DRIVER.quit();
        }
    }
}