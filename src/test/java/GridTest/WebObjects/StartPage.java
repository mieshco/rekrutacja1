package GridTest.WebObjects;

public enum StartPage implements Element_ {
    ONETRUST_POLICY_ACCEPT_BUTTON(".//div[@id = 'onetrust-button-group']/div/button", Type_.BUTTON),
    CONTAINER_SEARCH_INPUT(".//div[@class = 'inputcontainer']/input[contains(@placeholder,'numer') and contains(@placeholder,'przesyłki')]", Type_.CHECKBOX),
    CONTAINER_SEARCH_BUTTON(".//div[@class = 'btncontainer']/button/span[text()='Znajdź']", Type_.BUTTON);

    private final String name;
    private final String xpath;
    private final Type_ type;

    StartPage(String xpath, Type_ type) {
        this.name = name();
        this.xpath = xpath;
        this.type = type;
    }


    public String getXpath() {
        return this.xpath;
    }

    public String getName() {
        return this.name;
    }

    public Type_ getType() {
        return this.type;
    }
}
