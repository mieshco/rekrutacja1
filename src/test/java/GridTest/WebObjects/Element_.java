package GridTest.WebObjects;

public interface Element_ {
    String getXpath();

    String getName();
}
