package GridTest.WebObjects;

public enum StatusPage implements Element_ {
    CURRENT_STATE(".//div[@class = 'message-box messageBox']/p[contains(@class, '-big')]", Type_.LABEL),
    SCIEZKA_DO_PODSTRONY(".//ul[@class='breadcrumb-list']", Type_.LINK),
    ETYKIETA_NUMER_PRZESYLKI(".//div/h3/b", Type_.LABEL),
    POLE_POMOC(".//h2[text()='Pomoc']/..", Type_.CONTAINER),
    POLE_MENU(".//nav[@role='menubar']", Type_.CONTAINER),
    POLE_STOPKA(".//footer", Type_.CONTAINER);

    private final String name;
    private final String xpath;
    private final Type_ type;

    StatusPage(String xpath, Type_ type) {
        this.name = name();
        this.xpath = xpath;
        this.type = type;
    }


    public String getXpath() {
        return this.xpath;
    }

    public String getName() {
        return this.name;
    }

    public Type_ getType() {
        return this.type;
    }
}
