package GridTest.TestObjects.PackageDetails;

public class TargetMachineDetail {
    private String name;
    private String openingHours;
    private String locationDescription;
    private Double location_latitude;
    private Double location_longitude;
    private String address_line1;
    private String address_line2;
    // private String type;
    private Boolean location247;

    public TargetMachineDetail(String name, String openingHours, String locationDescription,
                               Double location_latitude, Double location_longitude,
                               String address_line1, String address_line2, Boolean location247) {
        this.name = name;
        this.openingHours = openingHours;
        this.locationDescription = locationDescription;
        this.location_latitude = location_latitude;
        this.location_longitude = location_longitude;
        this.address_line1 = address_line1;
        this.address_line2 = address_line2;

        this.location247 = location247;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public Double getLocation_latitude() {
        return location_latitude;
    }

    public void setLocation_latitude(Double location_latitude) {
        this.location_latitude = location_latitude;
    }

    public Double getLocation_longitude() {
        return location_longitude;
    }

    public void setLocation_longitude(Double location_longitude) {
        this.location_longitude = location_longitude;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public Boolean getLocation247() {
        return location247;
    }

    public void setLocation247(Boolean location247) {
        this.location247 = location247;
    }
}
