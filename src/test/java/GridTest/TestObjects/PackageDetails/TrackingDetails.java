package GridTest.TestObjects.PackageDetails;

public class TrackingDetails {
    private String status;
    private String originStatus;
    private String datetime;

    public TrackingDetails(String status, String originStatus, String datetime) {
        this.status = status;
        this.originStatus = originStatus;

        this.datetime = datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOriginStatus() {
        return originStatus;
    }

    public void setOriginStatus(String originStatus) {
        this.originStatus = originStatus;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
