package GridTest.TestObjects.PackageDetails;

public class CustomAttributes {
    private String size;
    private String targetMachineId;
    private Boolean endOfWeekCollection;
    private TargetMachineDetail targetMachineDetail;

    public CustomAttributes(String size, String targetMachineId, Boolean endOfWeekCollection,
                            TargetMachineDetail targetMachineDetail) {
        this.size = size;
        this.targetMachineId = targetMachineId;
        this.endOfWeekCollection = endOfWeekCollection;
        this.targetMachineDetail = targetMachineDetail;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTargetMachineId() {
        return targetMachineId;
    }

    public void setTargetMachineId(String targetMachineId) {
        this.targetMachineId = targetMachineId;
    }

    public Boolean getEndOfWeekCollection() {
        return endOfWeekCollection;
    }

    public void setEndOfWeekCollection(Boolean endOfWeekCollection) {
        this.endOfWeekCollection = endOfWeekCollection;
    }

    public TargetMachineDetail getTargetMachineDetail() {
        return targetMachineDetail;
    }

    public void setTargetMachineDetail(TargetMachineDetail targetMachineDetail) {
        this.targetMachineDetail = targetMachineDetail;
    }
}
