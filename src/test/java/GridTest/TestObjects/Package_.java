package GridTest.TestObjects;

import GridTest.TestObjects.PackageDetails.CustomAttributes;
import GridTest.TestObjects.PackageDetails.TrackingDetails;

import java.util.List;

public class Package_ {
    private String trackingNumber;
    private String service;
    private String type;
    private String status;
    private CustomAttributes customAttributes;
    private List<TrackingDetails> trackingDetails;
    //   private String expectedFlow;
    private String createdAt;
    private String updatedAt;

    public Package_(String trackingNumber, String service, String type,
                    String status, CustomAttributes customAttributes,
                    List<TrackingDetails> trackingDetails,
                    String createdAt, String updatedAt) {
        this.trackingNumber = trackingNumber;
        this.service = service;
        this.type = type;
        this.status = status;
        this.customAttributes = customAttributes;
        this.trackingDetails = trackingDetails;
        //    this.expectedFlow = expectedFlow;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CustomAttributes getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(CustomAttributes customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<TrackingDetails> getTrackingDetails() {
        return trackingDetails;
    }

    public void setTrackingDetails(List<TrackingDetails> trackingDetails) {
        this.trackingDetails = trackingDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
