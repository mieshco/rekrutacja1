package GridTest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources/Features/", tags = {"@CreateAbstract"},
        plugin = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber.json",
                "junit:target/cucumber.xml", "return:target/rerun.txt"},
        glue = {"StepDefinition"}
)
public class TestRunner {


}
