# language: pl
Funkcja: Sprawdzenie funkcjonalności wyszukiwania statusu paczki po numerze i poprawności wyświetlania strony ze statusem

  Szablon scenariusza: Wyszukuję paczkę po numerze i sprawdzam jej status
    Mając Otwartą stronę inpostu
    Kiedy wyszukuję przesyłkę <nr_przesylki>
    Wtedy sprawdzam czy faktyczny status jest równy <oczekiwany_status>

    Przykłady:
      | nr_przesylki             | oczekiwany_status |
      | 675220159157550132544277 | Dostarczona.      |
      | 602677159157340025039205 | Dostarczona.      |
      | 502677159157340025039205 | Dostarczona.      |

  Scenariusz: Wyszukuję paczkę po numerze i sprawdzam zawartość strony ze statusem
    Mając Otwartą stronę inpostu
    Kiedy wyszukuję przesyłkę 675220159157550132544277
    Wtedy sprawdzam czy adres jest zgodny z oczekiwanym https://inpost.pl/sledzenie-przesylek?number=675220159157550132544277
    I sprawdzam obecnosc elementu SCIEZKA_DO_PODSTRONY
    I sprawdzam czy tekst elementu ETYKIETA_NUMER_PRZESYLKI jest równy 675220159157550132544277
    I sprawdzam obecnosc elementu POLE_POMOC
    I sprawdzam obecnosc elementu POLE_MENU
    I sprawdzam obecnosc elementu POLE_STOPKA