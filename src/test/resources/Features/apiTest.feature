#language: pl
Funkcja: Pobranie informacji o przesyłce przez API


  Szablon scenariusza: Pobranie informacji o przesyłce i sprawdzenie zgodności z szablonem.

    Mając wszystkie dane potrzebne do wysłania zapytania poprzez API do serwisu shipx-proxy
    Kiedy wysyłam zapytanie GET z numerem przesyłki <nr_przesylki>
    Wtedy dostaję odpowiedź z serwisu ze statusem 200
    I odpowiedź jest w formie oczekiwanej
    I przesyłka jest w statusie <oczekiwany_status>

    Przykłady:
      | nr_przesylki             | oczekiwany_status |
      | 675220159157550132544277 | delivered         |
      | 602677159157340025039205 | delivered         |
      | 502677159157340025039205 | delivered         |